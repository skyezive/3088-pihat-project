# 3088 PiHat Project

Description of PiHat:

Our concept is to create a UPS (uninterrupted power supply) microPi HAT. The PiHAT will attach on top of a Raspberry Pi Zero and allow it to keep the Pi running during temporary power outages and loadshedding as well as keeping the Pi alive for long enough to shut it
down properly during extended blackouts. 

This microHat will attach to the Pi’s GPIO header pin 2/4 and the ground header pin to power the Pi. The microHat will have battery connectors where you will connect the battery pack cables to the on cables from the battery connectors.

The PiHAT will be very useful to anyone who wants to prevent damage to their Pi’s SDcard that can be caused by unexpected blackouts. This can include those using their Pi’s as home servers, storage devices, media servers, as well as those who intend to keep their Pi’s powered on throughout periods of loadshedding.


Bill of Materials for Power Circuitry Submodule:

3 resistors 
3 transistors 
3 zener diodes 
